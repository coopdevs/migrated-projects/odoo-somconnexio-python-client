tox==3.25.0
requests==2.27.1
mock==4.0.3
unittest2==1.1.0
pytest==6.2.5
pytest-cov==3.0.0
pytest-recording==0.12.0
factory_boy==3.2.1
flake8==4.0.1
flake8-quotes==3.3.1
