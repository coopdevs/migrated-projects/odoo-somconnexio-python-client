# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
## [0.1.21] - 2022-06-01
### Added
* Add addresses to Partner object returned in get methods. [#24](https://gitlab.com/coopdevs/odoo-somconnexio-python-client/-/merge_requests/24)

## [0.1.20] - 2022-04-20
### Added
* Add check_sponsor method to Partner resource [#32](https://gitlab.com/coopdevs/odoo-somconnexio-python-client/-/merge_requests/32)

### Removed
* Remove Tryton support [#31](https://gitlab.com/coopdevs/odoo-somconnexio-python-client/-/merge_requests/31)

## [0.1.19] - 2022-04-11
### Added
* Add sponsor data to Partner resource [#29](https://gitlab.com/coopdevs/odoo-somconnexio-python-client/-/merge_requests/29)
* Add filter to Product Catalog endpoint to select the products available from product code to change tariff [#30](https://gitlab.com/coopdevs/odoo-somconnexio-python-client/-/merge_requests/30)

## [0.1.18] - 2022-03-22
### Added
* Add language to ProductCatalog resource. [#26](https://gitlab.com/coopdevs/odoo-somconnexio-python-client/-/merge_requests/26)

## [0.1.17] - 2021-06-28
### Added
* Add resource to get the tariffs endpoint. [#20](https://gitlab.com/coopdevs/odoo-somconnexio-python-client/-/merge_requests/20)

## [0.1.16] - 2021-06-10
### Changed
* Add accept-language header when asking the discovery-channel endpoint. [#19](https://gitlab.com/coopdevs/odoo-somconnexio-python-client/-/merge_requests/19)

## [0.1.15] - 2021-04-08
### Fixed
* Send the SC ICC also in a new lines [#18](https://gitlab.com/coopdevs/odoo-somconnexio-python-client/-/merge_requests/18)

## [0.1.14] - 2021-03-15
### Fixed
* Concat second lastname to portability contract data [#16](https://gitlab.com/coopdevs/odoo-somconnexio-python-client/-/merge_requests/16)

## [0.1.13]
### Fixed
* Use the company_name and company_email in company SR [#15](https://gitlab.com/coopdevs/odoo-somconnexio-python-client/-/merge_requests/15)

## [0.1.12]
### Fixed
* Set company fields in SR creation [#14](https://gitlab.com/coopdevs/odoo-somconnexio-python-client/-/merge_requests/14)

## [0.1.11]
### Added
* Add member field to the Partner resource. [#13](https://gitlab.com/coopdevs/odoo-somconnexio-python-client/-/merge_requests/10)

### Changed
* Use pytest-recording instead of pytest-vrc for testing HTTP requests. [#12](https://gitlab.com/coopdevs/odoo-somconnexio-python-client/-/merge_requests/12)

## [0.1.10]
### Fixed
* Send always a delivery address in CRMLead creation. [#11](https://gitlab.com/coopdevs/odoo-somconnexio-python-client/-/merge_requests/11)

## [0.1.9]
### Added
* Set coop_candidate field to the Partner resource. [#10](https://gitlab.com/coopdevs/odoo-somconnexio-python-client/-/merge_requests/10)

## [0.1.8]
### Added
* Send the different invoice address with the ISPInfo. [#6](https://gitlab.com/coopdevs/odoo-somconnexio-python-client/-/merge_requests/6) and [#9](https://gitlab.com/coopdevs/odoo-somconnexio-python-client/-/merge_requests/9)
### Fixed
* Black lint [#8](https://gitlab.com/coopdevs/odoo-somconnexio-python-client/-/merge_requests/8)

## [0.1.7]
### Fixed
* Use fiber instead of fibre in previus_service of CRMLead

## [0.1.6]
### Removed
* Remove birthdate info from Partner resource [#5](https://gitlab.com/coopdevs/odoo-somconnexio-python-client/-/merge_requests/5)

## [0.1.5]
### Fixed
* Fix the tests of SubscriptionRequestFromPartnerForm.

## [0.1.4]
### Added
* Add SR info to the mapper output: Gender, Firstname, Lastname, IsCompany, CompanyName, Birthdate and Phone.

## [0.1.3]
### Added
* Add sponsor and coop agreement data to Partner response. [#4](https://gitlab.com/coopdevs/odoo-somconnexio-python-client/-/merge_requests/4)

## [0.1.2]
### Added
* Add the Discovery Channel resource and use it in the SR creation. [#3](https://gitlab.com/coopdevs/odoo-somconnexio-python-client/-/merge_requests/3)
### Fixed
* Cast previous provider from string to int in CRMLead creation call.

## [0.1.1]
### Fixed
* Cast voluntary contribution from string to int in SR creation call. [#2](https://gitlab.com/coopdevs/odoo-somconnexio-python-client/-/merge_requests/2)

## [0.1.0]
* Search Provider resource
* Create CRMLead resource
* Search and Get Partner resource
* Create SubscriptionRequest resource
* Initial commit with the SubscriptionRequest and the CRMLead resources.
  Also included the Client with get and post methods.
