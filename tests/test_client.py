from __future__ import unicode_literals  # support both Python2 and 3

from mock import patch
import unittest2 as unittest

import json
import os

from odoo_somconnexio_python_client.client import Client
from odoo_somconnexio_python_client import exceptions

from .settings import REQUIRED_ENVVARS


class FakeRequest:
    method = ""
    url = ""

    def __init__(self, method="GET", url="some-url"):
        self.method = method
        self.url = url


class FakeResponse:
    status_code = None
    content = ""
    reason = ""
    request = FakeRequest()

    def __init__(self, status=200, reason="", content="{}"):
        self.status_code = status
        self.content = content
        self.reason = reason

    def json(self):
        return json.loads(self.content)


@patch.dict("os.environ", REQUIRED_ENVVARS)
class ClientTests(unittest.TestCase):
    expected_headers = {
        "Accept": "application/json",
        "API-KEY": REQUIRED_ENVVARS.get("ODOO_APIKEY"),
    }
    sample_body = {"a": 1}
    sample_route = "/path"

    def test_init_has_not_envvars_defined_raises_exception(self):
        with self.assertRaises(Exception):
            for envvar in self.required_envvars.keys():
                os.unsetenv(envvar)
                Client()

    @patch(
        "odoo_somconnexio_python_client.client.requests.request", side_effect=Exception
    )
    def test_network_error_raises_expected_exception(self, _):
        with self.assertRaises(exceptions.HTTPError):
            Client().get(self.sample_route)

    @patch(
        "odoo_somconnexio_python_client.client.requests.request",
        return_value=FakeResponse(status=500),
    )
    def test_server_error_500_raises_expected_exception(self, request_mock):
        request_mock.return_value = FakeResponse(status=500)
        with self.assertRaises(exceptions.HTTPError):
            Client().get(self.sample_route)

    @patch(
        "odoo_somconnexio_python_client.client.requests.request",
        return_value=FakeResponse(),
    )
    def test_get(self, mock_request):
        Client().get(self.sample_route)

        mock_request.assert_called_once_with(
            "GET",
            "http://myoc/api/path",
            data=None,
            params={},
            headers=self.expected_headers,
        )

    @patch(
        "odoo_somconnexio_python_client.client.requests.request",
        return_value=FakeResponse(),
    )
    def test_post(self, mock_request):
        expected_headers = self.expected_headers.copy()
        expected_headers.update({"Content-Type": "application/json"})

        Client().post(self.sample_route, self.sample_body)

        mock_request.assert_called_once_with(
            "POST",
            "http://myoc/api/path",
            data=json.dumps(self.sample_body),
            params={},
            headers=expected_headers,
        )

    @patch(
        "odoo_somconnexio_python_client.client.requests.request",
        return_value=FakeResponse(),
    )
    def test_url_format(self, mock_request):
        os.environ["ODOO_BASEURL"] = "http://odoo.my//"
        Client().get(self.sample_route)

        mock_request.assert_called_once_with(
            "GET",
            "http://odoo.my/api/path",
            headers=self.expected_headers,
            data=None,
            params={},
        )
